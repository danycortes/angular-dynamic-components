import { Component, ChangeDetectionStrategy } from '@angular/core';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default
})


export class AppComponent {
    user: any = {
        name: 'Mark Hoppus',
        age: 44,
        location: 'California'
    };

    addProp() {
        this.user.email = 'honog_land@hotmail.com';
    }

    changeName() {
        this.user.name = 'Dany Cortes';
    }

    changeUser() {
        this.user = {
            name: 'Bombilla',
            age: 28,
            location: 'Mexico'
        };
    }
}
