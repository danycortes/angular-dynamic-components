import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AuthFormComponent } from './auth-form/auth-form.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { ThreeComponent } from './three/three.component';


@NgModule({
    declarations: [
        AppComponent,
        AuthFormComponent,
        OneComponent,
        TwoComponent,
        ThreeComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ],
    entryComponents: [
        AuthFormComponent
    ]
})


export class AppModule { }
