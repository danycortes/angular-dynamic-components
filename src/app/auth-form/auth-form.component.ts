import { Component, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss']
})


export class AuthFormComponent {
    title = 'Login';

    @Output() submitted: EventEmitter<any> = new EventEmitter();

    onSubmit( value ) {
        this.submitted.emit( value );
    }
}
