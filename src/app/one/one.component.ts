import { Component, Input, ChangeDetectionStrategy } from '@angular/core';


@Component({
    selector: 'example-one',
    templateUrl: './one.component.html',
    styleUrls: ['./one.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class OneComponent {
    @Input() user;

    update() {
        this.user.name = 'James H.';
    }
}
